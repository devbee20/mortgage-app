import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MortgageCalculatorService } from './mortgage-calculator/mortgage-calculator.service';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { MortgageCalculatorModule } from './mortgage-calculator/mortgage-calculator.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    SharedModule,
    MortgageCalculatorModule
  ],
  providers: [MortgageCalculatorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
