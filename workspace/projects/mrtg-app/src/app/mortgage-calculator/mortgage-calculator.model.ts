export interface MortgagePaymentModel {
  homePrice: number;
  downPmt: number;
  rate: number;
  period: number;
  insuranceAmt: number;
  mortgageRequired: number;
  downPmtPerc: number;
  monthlyPmt: number;
}
