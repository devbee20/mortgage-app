import { Injectable } from '@angular/core';

@Injectable()
export class MortgageCalculatorService {
  isMortgageInsured() {}

  getInsuranceRate() {}

  updateMortgage() {}

  getMortgageRequired() {}

  getInsuranceAmt() {}

  getMonthlyPmt() {}
}
