import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MortgageCalculatorService } from './mortgage-calculator.service';

@Component({
  selector: 'mrtg-mortgage-calculator',
  templateUrl: './mortgage-calculator.component.html',
  styleUrls: ['./mortgage-calculator.component.scss'],
})
export class MortgageCalculatorComponent implements OnInit {
  constructor(private appService: MortgageCalculatorService) {}
  public periods = [
    { value: 5, viewValue: '5 years' },
    { value: 10, viewValue: '10 years' },
    { value: 15, viewValue: '15 years' },
    { value: 20, viewValue: '20 years' },
    { value: 25, viewValue: '25 years' },
    { value: 30, viewValue: '30 years' },
  ];
  purchaseForm: FormGroup = {} as FormGroup;


  ngOnInit() {
   this.purchaseForm = new FormGroup({
      price: new FormControl(null, [Validators.required]),
      downPmt: new FormControl(null, [Validators.required]),
      rate: new FormControl(null, [Validators.required]),
      period: new FormControl(null, [Validators.required]),
    });

  }


}
