import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MortgageCalculatorService } from './mortgage-calculator.service';
import { MortgageCalculatorComponent } from './mortgage-calculator.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';
import { MaterialModule } from '../shared/materials/mat.module';
import { MatOptionModule } from '@angular/material/core';

@NgModule({
  declarations: [MortgageCalculatorComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    CoreModule,
  ],
  exports: [MortgageCalculatorComponent],
  providers: [MortgageCalculatorService],
})
export class MortgageCalculatorModule {}
