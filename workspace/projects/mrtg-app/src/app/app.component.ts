import { Component } from '@angular/core';

@Component({
  selector: 'mrtg-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mrtg-app';
}
